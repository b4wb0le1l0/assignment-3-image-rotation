#include "image.h"
#include "bmp.h"
#include "rotate.h"

#include <stdio.h>

int main(int argc, char **argv) {
    FILE *f_in;
    FILE *f_out;
    struct image image;
    struct image rotated_img;
    enum read_status readStatus;
    enum write_status writeStatus;
    enum rotate_status rotation = get_rotate(argv[3]);

    if (argc < 4) {
        fprintf(stderr, "Wrong number of input arguments.\n");
        return -1;
    }

    f_in = fopen(argv[1], "rb");
    if (!f_in) {
        fprintf(stderr, "Problems while opening input file.");
        return -1;
    }

    if (rotation == INVALID_INPUT) {
        fprintf(stderr, "Incorrect input, it can be int only.");
        fclose(f_in);
        return -1;
    }

    if (rotation == WRONG_ANGLE) {
        fprintf(stderr, "Wrong angle, it can be (0, 90, -90, 180, -180, 270, -270, 360, -360) only.");
        fclose(f_in);
        return -1;
    }

    readStatus = from_bmp(f_in, &image);
    fclose(f_in);

    if (readStatus != READ_OK) {
        fprintf(stderr, "Problem while reading input file.");
        delete_image(&image);
        return -1;
    }

    print_read_status(readStatus);
    printf("\n");

    rotated_img = rotationFunctions[rotation](&image);
    if (rotation != ROTATION_ERROR) {
        delete_image(&image);
    }

    f_out = fopen(argv[2], "wb");
    if (!f_out) {
        fprintf(stderr, "Problems while opening output file.");
        delete_image(&rotated_img);
        return -1;
    }

    writeStatus = to_bmp(f_out, &rotated_img);
    fclose(f_out);
    delete_image(&rotated_img);

    if (rotated_img.data == NULL) {fprintf(stderr, "Cleared successfully."); }
    else { fprintf(stderr, "Cleared isn't correctly."); }

    if (writeStatus != WRITE_OK) {
        fprintf(stderr, "Problem while writing output file.");
        return -1;
    }

    print_write_status(writeStatus);
    printf("\n");

    return 0;
}

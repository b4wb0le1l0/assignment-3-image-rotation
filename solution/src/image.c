#include "image.h"

#include <stdio.h>

struct image create_image(uint32_t width, uint32_t height)
{
    return (struct image) {
            .width = width,
            .height = height,
            .data = (struct pixel*) malloc(sizeof(struct pixel) * width * height)
    };
}

void delete_image(struct image *image) {
    if (image) {
        free(image->data);
        image->data = NULL;
        image->width = 0;
        image->height = 0;
    }
}

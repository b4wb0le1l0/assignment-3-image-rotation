#include "bmp.h"
#include "image.h"

#define BMP_STRUCTURE 0x4D42
#define BYTE_SIZE 40
#define BIT_COUNT 24

char *read_status[] =
        {
                [READ_OK] = "Image was read successfully.",
                [READ_IMAGE_ERROR] = "Problem while reading file.",
                [READ_INVALID_SIGNATURE] = "The problem with structure, this is not BMP file structure.",
                [READ_INVALID_BITS] = "The problem with bits, there is no 24 bit in the BMP file.",
                [READ_HEADER_ERROR] = "The problem with header of file.",
                [READ_INVALID_FILE] = "The problem with name of file."};

void print_read_status(enum read_status const readStatus)
{
    if (readStatus != READ_OK)
    {
        fprintf(stderr, "%s", read_status[readStatus]);
    }
    else
    {
        printf("%s", read_status[readStatus]);
    }
}

char *write_status[] =
        {
                [WRITE_OK] = "Image was written successfully.",
                [WRITE_ERROR] = "Problem while writing file."};

void print_write_status(enum write_status const writeStatus)
{
    if (writeStatus != WRITE_OK)
    {
        fprintf(stderr, "%s", write_status[writeStatus]);
    }
    else
    {
        printf("%s", write_status[writeStatus]);
    }
}

uint8_t get_padding(uint32_t const width)
{
    return (4 - (width * sizeof(struct pixel)) % 4) % 4;
}

int is_valid_header(struct bmp_header const *header)
{
    return header->biBitCount == BIT_COUNT && header->bfType == BMP_STRUCTURE;
}

enum read_status from_bmp(FILE *in, struct image *image)
{
    struct bmp_header header;

    if (!in)
        return READ_INVALID_FILE;

    if (!fread(&header, sizeof(struct bmp_header), 1, in) || !is_valid_header(&header))
    {
        return READ_HEADER_ERROR;
    }

    *image = create_image(header.biWidth, header.biHeight);

    if (!image->data)
    {
        return READ_IMAGE_ERROR;
    }

    uint8_t padding = get_padding(header.biWidth);

    for (size_t i = 0; i < header.biHeight; i++)
    {
        if (fread(&image->data[((header.biHeight - i - 1) * header.biWidth)], sizeof(struct pixel), image->width, in) != image->width)
        {
            delete_image(image);
            return READ_INVALID_BITS;
        }
        if (fseek(in, (long)padding, SEEK_CUR) != 0)
        {
            // обработка ошибки
        }
    }

    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *image)
{
    uint8_t padding = get_padding(image->width);
    struct bmp_header header;
    header.bfType = BMP_STRUCTURE;
    header.bfReserved = 0;
    header.biCompression = 0;
    header.biPlanes = 1;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = BYTE_SIZE;
    header.biBitCount = BIT_COUNT;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biSizeImage = (image->width + padding) * image->height * sizeof(struct pixel);
    header.bfileSize = (uint32_t)(sizeof(struct bmp_header) + header.biSizeImage);
    header.biWidth = image->width;
    header.biHeight = image->height;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    if (!(fwrite(&header, sizeof(struct bmp_header), 1, out)))
    {
        return WRITE_ERROR;
    }

    for (size_t i = 0; i < image->height; i++)
    {
        if (fwrite(&image->data[(image->height - i - 1) * image->width], sizeof(struct pixel), image->width, out) != image->width)
        {
            return WRITE_ERROR;
        }
        for (size_t j = 0; j < padding; j++)
        {
            fputc(0, out);
        }
    }
    return WRITE_OK;
}

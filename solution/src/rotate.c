#include "rotate.h"
#include "image.h"

#include <stdlib.h>
#include <string.h>

struct image create_rotated_image(size_t width, size_t height) {
    struct image rotated_image = create_image(width, height);

    if (!rotated_image.data) {
        rotated_image.width = 0;
        rotated_image.height = 0;
    }

    return rotated_image;
}

struct image rotate90(struct image const* image) {
    struct image out_image = create_rotated_image(image->height, image->width);

    for (size_t i = 0; i < image->height; i++) {
        for (size_t j = 0; j < image->width; j++) {
            out_image.data[j * image->height + (image->height - 1 - i)] = image->data[i * image->width + j];
        }
    }

    return out_image;
}

struct image rotate180(struct image const *image) {
    struct image out_image = create_rotated_image(image->width, image->height);

    for (size_t i = 0; i < image->height; i++) {
        for (size_t j = 0; j < image->width; j++) {
            out_image.data[(image->width - 1 - j) + (image->height - 1 - i) * image->width] = image->data[i * image->width + j];
        }
    }

    return out_image;
}

struct image rotate270(struct image const *image) {
    struct image out_image = create_rotated_image(image->height, image->width);

    for (size_t i = 0; i < image->height; i++) {
        for (size_t j = 0; j < image->width; j++) {
            out_image.data[(image->width - 1 - j) * image->height + i] = image->data[j + i * image->width];
        }
    }

    return out_image;
}

struct image rotate360(struct image const *image) {
    struct image out_image = create_rotated_image(image->width, image->height);

    memcpy(out_image.data, image->data, sizeof(struct pixel) * image->width * image->height);

    return out_image;
}

enum rotate_status get_rotate(char const* angleString) {
    char *edge;
    long angle = strtol(angleString, &edge, 10);
    if (*edge == '\0') {
        switch (angle) {
            case 90:
            case -270:
                return ROTATION_90;
            case 180:
            case -180:
                return ROTATION_180;
            case 270:
            case -90:
                return ROTATION_270;
            case 360:
            case -360:
            case 0:
                return ROTATION_360;
            default:
                return WRONG_ANGLE;
        }
    } else {
        if (*edge == '-' && *(edge + 1) == '\0') {
            return INVALID_INPUT_NEGATIVE;
        } else {
            return INVALID_INPUT;
        }
    }
}

rotate rotationFunctions[] = {
        [ROTATION_90] = rotate90,
        [ROTATION_180] = rotate180,
        [ROTATION_270] = rotate270,
        [ROTATION_360] = rotate360
};

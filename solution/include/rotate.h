#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H

#include "image.h"
#include <stdio.h>

typedef struct image (*rotate) (struct image const*);

enum rotate_status {
    ROTATION_90,
    ROTATION_180,
    ROTATION_270,
    ROTATION_360,
    INVALID_INPUT,
    INVALID_INPUT_NEGATIVE,
    WRONG_ANGLE,
    ROTATION_ERROR
};

enum rotate_status get_rotate(char const* angleString);

extern rotate rotationFunctions[];

#endif //IMAGE_TRANSFORMER_ROTATE_H

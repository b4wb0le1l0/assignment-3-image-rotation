#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H

#include "image.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

enum read_status
{
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_HEADER_ERROR,
    READ_IMAGE_ERROR,
    READ_INVALID_FILE
};

enum  write_status
{
    WRITE_OK = 0,
    WRITE_ERROR
};

extern char* read_status[];
extern char* write_status[];

void print_read_status(enum read_status const readStatus);
void print_write_status(enum write_status const writeStatus);

enum read_status from_bmp(FILE* in, struct image* image);
enum write_status to_bmp(FILE* out, struct image const* image);

uint8_t get_padding(uint32_t width);

#endif //IMAGE_TRANSFORMER_BMP_H
